export const QuestionDifficulty = {
  Easy: "آسان",
  Medium: "متوسط",
  Hard: "سخت",
}

export const QuestionType = {
  MultiOption: 'چند گزینه ای',
  TrueFalse: 'صحیح و خطا',
  MultiAnswer: 'چند جوابی',
  ImageQuestion: 'تصویری',
  VideoQuestion: 'ویدئویی',
  ListMatch: 'تطبیق لیست',
  ListOrder: 'ترتیب لیست',
  BlankQuestion: 'تکمیل جای خالی',
  Descreptive: 'تشریحی',
  Practical: 'عملی',
}