import React, { Component } from "react";
import { create } from 'jss';
import rtl from 'jss-rtl';
import {
  StylesProvider, ThemeProvider, createMuiTheme, jssPreset
} from '@material-ui/core/styles';
import Question from "./containers/Question/Question";
import Arrow from './components/Arrow/Arrow';

//========================================================
// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });
const theme = createMuiTheme({ direction: "rtl", });
class App extends Component {
  render() {
    return (
      <StylesProvider jss={jss}>
        <ThemeProvider theme={theme}>
          <Question></Question>
        </ThemeProvider>
      </StylesProvider>
      // <Tree />

    )
  }

}
export default App;

