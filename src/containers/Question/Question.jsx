import React, { useState } from "react";
import ItemBox from "../../components/QuestionTypeBox/ItemBox";
import { Button } from '@material-ui/core';
import MultiOption from "../../components/QuestionTypes/MultiOption";
import ListMatch from "../../components/QuestionTypes/ListMatch";
import { useForm } from "react-hook-form";

//========================================================
export default (props) => {
  const FormHook = useForm();
  const initQuestion = {
    type: "",
    text: "",
    tags: [],
    difficulty: "",
    time: "",
    options: [],
    list1: [],
    list2: []
  };
  const [questionState, setQuestionState] = useState(initQuestion);

  //#region handler functions
  const handleSelectQuestionType = (type) => {
    const question = JSON.parse(JSON.stringify(questionState));
    question.type = type;
    setQuestionState(question);
  }
  const optionsCorrectionHandler = (index) => {
    const question = JSON.parse(JSON.stringify(questionState));
    const option = question.options[index];
    option.isCorrect = !option.isCorrect;
    setQuestionState(question)
  }
  const optionsTextHandler = (e, index, fieldName) => {
    const question = JSON.parse(JSON.stringify(questionState));
    const option = question.options[index];
    option[fieldName] = e.currentTarget.value;
    setQuestionState(question)
  }
  const addOptionHandler = () => {
    const question = JSON.parse(JSON.stringify(questionState));
    question.options.push({
      text: "",
      isCorrect: false,
      textEquevalent: "",
      order: ""
    });
    setQuestionState(question)
  }
  const handleSelectDifficulty = (event) => {
    const question = JSON.parse(JSON.stringify(questionState));
    question.difficulty = event.target.value;
    setQuestionState(question)
  }
  const handleChangeQuestionInputs = (key, val) => {
    const question = JSON.parse(JSON.stringify(questionState));
    question[key] = val;
    setQuestionState(question)
  }
  //---------------
  const add2ListHandler = (listKey) => {
    const question = JSON.parse(JSON.stringify(questionState));
    question[listKey].push('');
    setQuestionState(question)
  }
  const listTextHandler = (e, listKey, index) => {
    const question = JSON.parse(JSON.stringify(questionState));
    question[listKey][index] = e.currentTarget.value;
    setQuestionState(question)
  }
  //------------------
  const functionList = {
    handleSelectDifficulty: handleSelectDifficulty,
    handleChangeQuestionInputs: handleChangeQuestionInputs,
    optionsCorrectionHandler: optionsCorrectionHandler,
    optionsTextHandler: optionsTextHandler,
    addOptionHandler: addOptionHandler,
    add2ListHandler: add2ListHandler,
    listTextHandler: listTextHandler,
  };
  //#endregion

  //#region renderin functions
  const renderQuestion = () => {
    switch (questionState.type) {
      case "MultiOption":
        return <MultiOption question={questionState} funcs={functionList} FormHook={FormHook} />
      case "ListMatch":
        return <ListMatch question={questionState} funcs={functionList} FormHook={FormHook} />
      default: return null;
    }
  }
  //#endregion
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  return (
    <div style={{ marginLeft: 100, marginRight: 100, direction: "rtl", }}>
      <ItemBox handleSelectQuestionType={handleSelectQuestionType} selected={questionState.type} />
      <br />
      {renderQuestion()}
      <br />
      <Button variant="contained" color="primary" type="submit"
        onClick={FormHook.handleSubmit(data => {
          console.log(data);
          console.log(questionState);
        })}>submit</Button>
    </div>
  )
}
