import React from 'react';
import { Check, Clear } from '@material-ui/icons';
import SvgIcon from '@material-ui/core/SvgIcon';
import { fade, makeStyles, withStyles } from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import TreeItem from '@material-ui/lab/TreeItem';



const StyledTreeItem = withStyles((theme) => ({
  iconContainer: {
    '& .close': {
      opacity: 0.3,
    },
  },
  group: {
    marginLeft: 7,
    paddingLeft: 18,
    borderLeft: `1px dashed ${fade(theme.palette.text.primary, 0.4)}`,
  },
}))((props) => <TreeItem {...props} />);//TransitionComponent={TransitionComponent}

const useStyles = makeStyles({
  root: {
    height: 264,
    flexGrow: 1,
    maxWidth: 400,
  },
});

export default function CustomizedTreeView() {
  const classes = useStyles();

  const data = [
    {
      name: 'math',
      children: [
        { name: 'math1' },
        { name: 'math2', id: 12 }
      ]
    },
    {
      name: 'sport',
      children: [
        { name: 'swim', id: 21, children: [{ name: 'master swim' }] }
      ]
    }
  ]

  const renderTree = (node) => {
    const nodeId = node.id || Math.random(1000000);
    return <StyledTreeItem nodeId={`${nodeId}`} key={nodeId}
      label={node.name}
    // labelText="Social"
    // labelIcon={Clear}
    // labelInfo="90"
    // {<Check />}
    >
      {Array.isArray(node.children) ? node.children.map(child => renderTree(child)) : null}
    </StyledTreeItem >
  }


  return (
    <TreeView
      className={classes.root}
      defaultExpanded={['1']}
      defaultCollapseIcon={
        <SvgIcon fontSize="inherit" style={{ width: 14, height: 14 }}>
          <path d="M22.047 22.074v0 0-20.147 0h-20.12v0 20.147 0h20.12zM22.047 24h-20.12q-.803 0-1.365-.562t-.562-1.365v-20.147q0-.776.562-1.351t1.365-.575h20.147q.776 0 1.351.575t.575 1.351v20.147q0 .803-.575 1.365t-1.378.562v0zM17.873 11.023h-11.826q-.375 0-.669.281t-.294.682v0q0 .401.294 .682t.669.281h11.826q.375 0 .669-.281t.294-.682v0q0-.401-.294-.682t-.669-.281z" />
        </SvgIcon>
      }
      defaultExpandIcon={
        <SvgIcon fontSize="inherit" style={{ width: 14, height: 14 }}>
          <path d="M22.047 22.074v0 0-20.147 0h-20.12v0 20.147 0h20.12zM22.047 24h-20.12q-.803 0-1.365-.562t-.562-1.365v-20.147q0-.776.562-1.351t1.365-.575h20.147q.776 0 1.351.575t.575 1.351v20.147q0 .803-.575 1.365t-1.378.562v0zM17.873 12.977h-4.923v4.896q0 .401-.281.682t-.682.281v0q-.375 0-.669-.281t-.294-.682v-4.896h-4.923q-.401 0-.682-.294t-.281-.669v0q0-.401.281-.682t.682-.281h4.923v-4.896q0-.401.294-.682t.669-.281v0q.401 0 .682.281t.281.682v4.896h4.923q.401 0 .682.281t.281.682v0q0 .375-.281.669t-.682.294z" />
        </SvgIcon>
      }
    >
      {data.map(x => renderTree(x))}
    </TreeView>
  );
}

