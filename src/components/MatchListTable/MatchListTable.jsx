import React from 'react';
import { Grid, IconButton, TextField } from '@material-ui/core';
import { AddBox } from '@material-ui/icons';

//======================================
export default (props) => {
  return (

    <Grid spacing={3} container variant="outlined">
      <Grid item xs={3}>
        <IconButton onClick={() => props.add2ListHandler(props.name)} component="span"> <AddBox /> </IconButton>
      </Grid>
      <Grid item xs={9}>
        {props.texts.map((x, index) =>
          <TextField name="listText" fullWidth={true} variant="outlined"
            key={index} value={x} margin="dense"
            onChange={(e) => props.listTextHandler(e, props.name, index)}
          />
        )}
      </Grid>
    </Grid>
  )
}