import React from "react";
import OptionInput from './OptionInput/OptionInput';
import { Grid, Button } from '@material-ui/core';
import { AddBox } from '@material-ui/icons';

//====================================
export default (props) => {
  return (
    <Grid spacing={3} container>
      {/* add option */}
      <Grid item xs={4}>
        <Button fullWidth={true} variant="outlined" startIcon={<AddBox />}
          onClick={props.addOptionHandler}
        >{props.title}</Button>
      </Grid>
      {props.options.map((option, index) =>
        <Grid item xs={4} key={index}>
          <OptionInput isCorrect={option.isCorrect} text={option.text}
            changeCorrectionHandler={() => props.changeCorrectionHandler(index)}
            optionsTextHandler={(e) => props.optionsTextHandler(e, index, 'text')}
            FormHook={props.FormHook}
          ></OptionInput>
        </Grid>
      )}
    </Grid>
  )
}
