import React from 'react';
import { Check, Clear } from '@material-ui/icons';
import { green } from '@material-ui/core/colors';
import { TextField } from '@material-ui/core';
import QuestionValidator from '../../QuestionTypes/QuestionValidator';

//=======================================
export default (props) => {
  const optionIcon = (
    <div onClick={props.changeCorrectionHandler} style={{ cursor: "pointer" }}>
      {props.isCorrect ? <Check style={{ color: green[500] }} /> : <Clear color="error" />}
      {/* <select className="fa" style={{ border: "none", color: "red" }}>
        <option value="false" style={{ color: "red" }}>&#xf00d;</option>
        <option value="true" style={{ color: "green" }}>&#xf00c;</option>
        <option value="delete">&#xf1f8;</option>
      </select> */}
    </div>
  )
  return (
    <TextField name="optionText" fullWidth={true} placeholder="پاسخ" variant="outlined"
      value={props.text}
      onChange={props.optionsTextHandler}
      multiline
      rows={1}
      rowsMax={3}
      InputProps={{ endAdornment: optionIcon }}
      inputRef={props.FormHook.register(QuestionValidator.optionText())}
      helperText={props.FormHook.errors.optionText && props.FormHook.errors.optionText.message}
      error={props.FormHook.errors.optionText ? true : false}
    />

    // <div className="row" style={{ bodrder: "1px solid gray" }}>
    //   <div className="col-sm-9">
    //     <TextField fullWidth={true} xs={3} border={0} />

    //   </div>
    //   <div className="col-sm-3" onClick={props.handleCorrection}>
    //     {props.isCorrect ? <Check style={{ color: green[500] }} /> : <Clear color="error" />}
    //   </div>
    // </div>
  )
}