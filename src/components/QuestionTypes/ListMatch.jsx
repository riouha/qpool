import React from 'react';
import MatchListTable from '../MatchListTable/MatchListTable';
import QuestionValidator from './QuestionValidator';
import { QuestionDifficulty } from '../../Enums/QuestionDifficulty';
import {
  Select, MenuItem, TextField, Grid,
  InputLabel, FormControl, FormHelperText
} from '@material-ui/core';
import { AccessAlarm } from '@material-ui/icons';
import ChipInput from 'material-ui-chip-input';
import { Controller } from "react-hook-form";
import Arrow from '../Arrow/Arrow';
//=====================================
export default (props) => {
  return (
    <form className="container" noValidate
      autoComplete="off" style={{ textAlign: "right" }}>
      <Grid spacing={3} container>

        {/* tag */}
        <Grid item xs={4}>
          <ChipInput name="tags" fullWidth={true} variant="outlined" label="تگ"
            defaultValue={props.question.tags}
            // onUpdateInput={(x, y) => {
            //   //search
            //   console.log(x.currentTarget.value);
            // }}
            onChange={tags => props.funcs.handleChangeQuestionInputs('tags', tags)}
          />
        </Grid>

        {/* difficulty */}
        <Grid item xs={4}>
          <FormControl variant="outlined"
            fullWidth={true} label="درجه سختی سوال"
          >
            <InputLabel id="difficulty-label">درجه سختی سوال</InputLabel>
            <Controller
              as={
                <Select value={props.question.difficulty}
                  labelId="difficulty-label" onChange={props.funcs.handleSelectDifficulty}
                >
                  {Object.keys(QuestionDifficulty).map(x =>
                    <MenuItem value={x} key={x}>{QuestionDifficulty[x]}</MenuItem>
                  )}
                </Select>
              }
              name="difficulty"
              rules={QuestionValidator.difficulty()}
              error={props.FormHook.errors.difficulty ? true : false}
              control={props.FormHook.control}
              defaultValue=""
            />
            <FormHelperText error>
              {props.FormHook.errors.difficulty && props.FormHook.errors.difficulty.message}
            </FormHelperText>
          </FormControl>
        </Grid>

        {/* time */}
        <Grid item xs={4}>
          <TextField name="time" fullWidth={true} label="زمان پاسخگویی" variant="outlined"
            value={props.question.time}
            onChange={(e) => props.funcs.handleChangeQuestionInputs('time', e.currentTarget.value)}
            InputProps={{ endAdornment: <AccessAlarm /> }}
            inputRef={props.FormHook.register(QuestionValidator.time())}
            helperText={props.FormHook.errors.time && props.FormHook.errors.time.message}
            error={props.FormHook.errors.time ? true : false}
          />
        </Grid>

        {/* text */}
        <Grid item xs={8}>
          <TextField fullWidth={true} name="text" label="صورت سوال" variant="outlined" dir="rtl"
            value={props.question.text}
            onChange={(e) => props.funcs.handleChangeQuestionInputs('text', e.currentTarget.value)}
            multiline
            rows={4}
            rowsMax={8}
            inputRef={props.FormHook.register(QuestionValidator.text())}
            helperText={props.FormHook.errors.text && props.FormHook.errors.text.message}
            error={props.FormHook.errors.text ? true : false}
          />
        </Grid>
        <Grid item xs={4} />

        {/* table */}
        <Grid item xs={4}>
          <MatchListTable name="list1"
            texts={props.question.list1}
            add2ListHandler={props.funcs.add2ListHandler}
            listTextHandler={props.funcs.listTextHandler}
          // FormHook={props.FormHook}
          />
        </Grid>
        <Grid item xs={4} >
          <div style={{ marginTop: '20px' }}>
            {props.question.list1.map((x, index) => {
              if (props.question.list2[index] !== null && props.question.list2[index] !== undefined)
                return <div style={{ marginBottom: '32px' }}>
                  <Arrow></Arrow>
                </div>
            })}
          </div>
        </Grid>
        <Grid item xs={4}>
          <MatchListTable name="list2"
            texts={props.question.list2}
            add2ListHandler={props.funcs.add2ListHandler}
            listTextHandler={props.funcs.listTextHandler}
          />
        </Grid>

      </Grid>
    </form>
  )
}