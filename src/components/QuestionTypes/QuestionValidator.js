const text = () => {
  return {
    required: {
      value: true,
      message: "صورت سوال را وارد نمایید."
    }
  }
}
const difficulty = () => {
  return {
    required: {
      value: true,
      message: "میزان سختی سوال را انتخاب کنید."
    }
  }
}
const time = () => {
  return {
    required: {
      value: true,
      message: "مدت زمان پاسخگویی را وارد کنید."
    },
    pattern: {
      value: /^\d*\.?\d*$/,
      message: "زمان را در فرمت صحیح وارد کنید."
    },
  }
}

const optionText = () => {
  return {
    required: {
      value: true,
      message: "متن گزینه را وارد نمایید."
    }
  }
}

// const optionCorrection = () => {
//   return {
//     required: {
//       value: true,
//       message: "متن گزینه را وارد نمایید."
//     }
//   }
// }

export default {
  text, difficulty, time, optionText
}