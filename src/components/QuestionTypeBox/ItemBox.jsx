import React from 'react';
import { QuestionType } from '../../Enums/QuestionDifficulty';
import './ItemBox.css';
//============================================================
export default (props) => {
  return (
    <div className="row ItemBox">
      <div className="col-sm-3 text-center align-self-center">{/* justsify-content-end */}
        <h4>انواع سوال</h4>
      </div>
      <div className="col-sm-9" >
        <div className="row">
          {Object.keys(QuestionType).map(key => <div key={key}
            onClick={() => props.handleSelectQuestionType(key)}
            className={key === props.selected ?
              "col-sm-2 m-2 BoxItems selected" : "col-sm-2 m-2 BoxItems"}>
            {QuestionType[key]}
          </div>)}
        </div>

      </div>

    </div>
  )
}
